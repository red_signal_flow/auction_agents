package model.validate_helper;

import exceptions.ValidateException;
import model.Category;

public class CategoryValidateHelper {

	public static boolean isValid(Category category) {
		boolean valid;
		valid = isNameUnique(category);
		valid = valid && isNameValid(category);
		return valid;
	}
	
	public static void validate(Category category) throws ValidateException {
		validateNameUnique(category);
		validateName(category);
	}
	
	public static boolean isNameValid(Category category) {
		if(category.getName().isEmpty()) {
			return false;
		}
		return true;
	}
	
	public static void validateName(Category category) throws ValidateException {
		if(!isNameValid(category)) {
			throw new ValidateException("Name {" + category.getName() + "} is invalid");
		}
	}
	
	public static boolean isNameUnique(Category category) {
		for (Category otherCategory : Category.getAll()) {
			if (category.getName().equals(otherCategory.getName())) {
				return false;
			}
		}
		return true;
	}
	
	public static void validateNameUnique(Category category) throws ValidateException {
		if(!isNameUnique(category)) {
			throw new ValidateException("Name {" + category.getName() + "} exists"); 
		}
	}
}
