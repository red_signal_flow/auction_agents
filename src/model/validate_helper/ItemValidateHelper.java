package model.validate_helper;

import model.Item;
import exceptions.ValidateException;

public class ItemValidateHelper {

	public static boolean isValid(Item item) {
		boolean valid;
		valid = isNameUnique(item);
		valid = valid && isNameValid(item);
		return valid;
	}
	
	public static void validate(Item item) throws ValidateException {
		validateNameUnique(item);
		validateName(item);
	}
	
	public static boolean isNameValid(Item item) {
		if(item.getName().isEmpty()) {
			return false;
		}
		return true;
	}
	
	public static void validateName(Item item) throws ValidateException {
		if(!isNameValid(item)) {
			throw new ValidateException("Name {" + item.getName() + "} is invalid");
		}
	}
	
	public static boolean isNameUnique(Item item) {
		for (Item otherItem : Item.getAll()) {
			if (item.getName().equals(otherItem.getName())) {
				return false;
			}
		}
		return true;
	}
	
	public static void validateNameUnique(Item item) throws ValidateException {
		if(!isNameUnique(item)) {
			throw new ValidateException("Name {" + item.getName() + "} exists"); 
		}
	}
}
