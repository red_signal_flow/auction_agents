package model.dao;

import java.util.ArrayList;

public abstract class BasicDAO<M> {

	private ArrayList<M> modelList;

	public BasicDAO() {
		this.modelList = new ArrayList<M>();
	}

	public void save(M model) {
		this.modelList.add(model);
	}

	public void remove(M model) {
		this.modelList.remove(model);
	}
	
	public ArrayList<M> getAll() {
		return this.modelList;
	}
	
	
}
