package model.dao;

import java.util.HashMap;

import model.Category;
import model.Item;

public class DAOHandler {

	public static DAOHandler instance;
	
	public static DAOHandler getInstance() {
		if(instance == null) {
			instance = new DAOHandler();
		}
		return instance;
	}
	
	public HashMap<Class<?>, BasicDAO<?> > daoMap;
	
	public DAOHandler() {
		
	}
	
	public BasicDAO<?> getDAO(String className) {
		Class<?> klass = null;
		try {
			klass = Class.forName("model." + className);
		} catch (ClassNotFoundException e) {
			System.out.println("ERROR: " + e.getMessage());
			e.printStackTrace();
		}
		return this.daoMap.get(klass);
	}
	
	public BasicDAO<?> getDAO(Class<?> klass) {
		return this.daoMap.get(klass);
	}
	
	public void initialize() {
		this.daoMap = new HashMap<Class<?>, BasicDAO<?> >();
		this.daoMap.put(Item.class, new ItemDAO());
		this.daoMap.put(Category.class, new CategoryDAO());
	}
}
