package model;

import java.util.ArrayList;

import model.dao.DAOHandler;
import model.dao.ItemDAO;
import model.validate_helper.ItemValidateHelper;
import exceptions.ValidateException;

public class Item {

	private String name;
	private Category category;
	private Float minimumPrice;
	
	private ItemDAO itemDAO;
	
	public Item() {
		this.name = "";
		this.category = null;
		this.minimumPrice = 0.0f;
		this.itemDAO = (ItemDAO) DAOHandler.getInstance().getDAO(this.getClass());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Float getMinimumPrice() {
		return minimumPrice;
	}

	public void setMinimumPrice(Float minimumPrice) {
		this.minimumPrice = minimumPrice;
	}
	
	public void save() throws ValidateException {
		ItemValidateHelper.validate(this);
		this.itemDAO.save(this);
	}

	public void destroy() {
		this.itemDAO.remove(this);
	}
	
	public static ArrayList<Item> getAll() {
		return ((ItemDAO)DAOHandler.getInstance().getDAO(Item.class)).getAll();
	}
	
	public static Integer getSize() {
		return ((ItemDAO)DAOHandler.getInstance().getDAO(Item.class)).getAll().size();
	}
	
	public static Item findByName(String name) {
		for(Item item : getAll()) {
			if(item.getName().equals(name)) {
				return item;
			}
		}
		
		return null;
	}
}
