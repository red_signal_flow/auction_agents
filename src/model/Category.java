package model;

import java.util.ArrayList;

import exceptions.ValidateException;
import model.dao.CategoryDAO;
import model.dao.DAOHandler;
import model.validate_helper.CategoryValidateHelper;

public class Category {

	private String name;
	
	private CategoryDAO categoryDAO;
	
	public Category() {
		this.name = "";
		this.categoryDAO = (CategoryDAO) DAOHandler.getInstance().getDAO(this.getClass());
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void save() throws ValidateException {
		CategoryValidateHelper.validate(this);
		this.categoryDAO.save(this);
	}

	public void destroy() {
		this.categoryDAO.remove(this);
	}
	
	public static ArrayList<Category> getAll() {
		return ((CategoryDAO)DAOHandler.getInstance().getDAO(Category.class)).getAll();
	}
	
	public static Integer getSize() {
		return ((CategoryDAO)DAOHandler.getInstance().getDAO(Category.class)).getAll().size();
	}
	
	public static Category findByName(String name) {
		for(Category category : getAll()) {
			if(category.getName().equals(name)) {
				return category;
			}
		}
		
		return null;
	}
}
