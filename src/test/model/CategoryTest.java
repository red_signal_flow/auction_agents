package test.model;

import static org.junit.Assert.assertEquals;
import model.Category;
import model.dao.DAOHandler;

import org.junit.Before;
import org.junit.Test;

import exceptions.ValidateException;

public class CategoryTest {

	@Before
	public void setup() {
		DAOHandler.getInstance().initialize();
	}

	@Test
	public void createCategory() throws Exception {
		Category category = new Category();
		category.setName("New Category");
		category.save();

		assertEquals(category, Category.getAll().get(0));
	}

	@Test
	public void createMoreCategories() throws Exception {
		Category category = new Category();
		category.setName("1 Category");
		category.save();
		category = new Category();
		category.setName("2 Category");
		category.save();
		category = new Category();
		category.setName("3 Category");
		category.save();

		assertEquals(3, Category.getSize().intValue());
	}

	@Test
	public void findCategoryByName() throws Exception {
		String name = "New Category";
		Category category = new Category();
		category.setName(name);
		category.save();

		assertEquals(category, Category.findByName(name));
	}

	@Test
	public void destroyCategory() throws Exception {
		Category category = new Category();
		category.setName("New Category");
		category.save();
		category.destroy();

		assertEquals(0, Category.getSize().intValue());
	}
	
	@Test(expected = ValidateException.class)
	public void dontCreateCategoryWithSameName() throws Exception {
		Category category = new Category();
		category.setName("New Category");
		category.save();
		category = new Category();
		category.setName("New Category");
		category.save();
	}
	
	@Test(expected = ValidateException.class)
	public void dontCreateCategoryWithoutName() throws Exception {
		Category category = new Category();
		category.save();
	}
}
