package test.model;

import static org.junit.Assert.assertEquals;
import model.Item;
import model.dao.DAOHandler;

import org.junit.Before;
import org.junit.Test;

import exceptions.ValidateException;

public class ItemTest {

	@Before
	public void setup() {
		DAOHandler.getInstance().initialize();
	}

	@Test
	public void createItem() throws Exception {
		Item item = new Item();
		item.setName("New Category");
		item.save();

		assertEquals(item, Item.getAll().get(0));
	}

	@Test
	public void createMoreItens() throws Exception {
		Item item = new Item();
		item.setName("1 Item");
		item.save();
		item = new Item();
		item.setName("2 Item");
		item.save();
		item = new Item();
		item.setName("3 Item");
		item.save();

		assertEquals(3, Item.getSize().intValue());
	}

	@Test
	public void findItemByName() throws Exception {
		String name = "New Item";
		Item item = new Item();
		item.setName(name);
		item.save();

		assertEquals(item, Item.findByName(name));
	}

	@Test
	public void destroyItem() throws Exception {
		Item item = new Item();
		item.setName("New Item");
		item.save();
		item.destroy();

		assertEquals(0, Item.getSize().intValue());
	}
	
	@Test(expected = ValidateException.class)
	public void dontCreateItemWithSameName() throws Exception {
		Item item = new Item();
		item.setName("New Item");
		item.save();
		item = new Item();
		item.setName("New Item");
		item.save();
	}
	
	@Test(expected = ValidateException.class)
	public void dontCreateItemWithoutName() throws Exception {
		Item item = new Item();
		item.save();
	}

}
