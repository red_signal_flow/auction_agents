package exceptions;

public class ValidateException extends Exception {

	private static final long serialVersionUID = -6685710198278689518L;

	public ValidateException() {
		super();
	}
	
	public ValidateException(String message) {
		super(message);
	}
}
